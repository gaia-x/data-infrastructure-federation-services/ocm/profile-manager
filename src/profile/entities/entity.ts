import { IsString, IsNotEmpty, IsDate } from 'class-validator';

export class ProfileDto {
  @IsString()
  @IsNotEmpty()
  id: string;

  @IsDate()
  @IsNotEmpty()
  profileDate: Date;
}
