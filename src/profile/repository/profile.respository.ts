import { Injectable } from '@nestjs/common';
import { PrismaService } from '../../prisma/prisma.service';
import { ProfileDto } from '../entities/entity';

@Injectable()
export class ProfileRepository {
  constructor(private readonly prismaService: PrismaService) {}

  async createProfile(profile: ProfileDto) {
    return await this.prismaService.profile.create({
      data: {
        profileDate: new Date(),
      },
    });
  }

  async findProfiles() {
    const getProfile = await this.prismaService.profile.findMany();
    return getProfile;
  }
}
