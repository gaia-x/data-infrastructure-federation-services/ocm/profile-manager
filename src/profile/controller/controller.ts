import {
  Body,
  Controller,
  Get,
  InternalServerErrorException,
  Post,
  Version,
} from '@nestjs/common';
import logger from '../../../lib/logger';
import { ProfilesService } from '../services/service';
import { ResponseType } from 'src/common/response';
import { statusCode } from '../../../src/common/status.codes';
import { ProfileDto } from '../entities/entity';

@Controller()
export class ProfilesController {
  constructor(private readonly profilesService: ProfilesService) {}

  @Version(['1'])
  @Post('profile')
  createProfiles(@Body() profile: ProfileDto) {
    try {
      logger.info('Profile created successfully');
      const res: ResponseType = {
        statusCode: statusCode.INSERT,
        message: 'Profile created successfully',
        data: this.profilesService.createProfiles(profile),
      };
      return res;
    } catch (error) {
      throw new InternalServerErrorException('some Error');
    }
  }

  @Version(['1'])
  @Get('profile')
  async findProfiles() {
    try {
      logger.info('Profiles fetch successfully');
      const res: ResponseType = {
        statusCode: statusCode.OK,
        message: 'Profiles fetch successfully',
        data: await this.profilesService.findProfiles(),
      };
      return res;
    } catch (error) {
      throw new InternalServerErrorException('some Error');
    }
  }
}
