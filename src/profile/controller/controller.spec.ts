import { Test, TestingModule } from '@nestjs/testing';
import { ProfilesController } from './controller';
import { ProfilesService } from '../services/service';
import { ConfigModule } from '@nestjs/config';
import { PrismaService } from '../../prisma/prisma.service';
import { ProfileDto } from '../entities/entity';

describe('ProfilesController', () => {
  let controller: ProfilesController;
  const connection = new ProfileDto();
  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      imports: [ConfigModule],
      controllers: [ProfilesController],
      providers: [ProfilesService, PrismaService],
      exports: [PrismaService],
    }).compile();

    controller = module.get<ProfilesController>(ProfilesController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });

  it('find All Profiles', async () => {
    const res = await controller.findProfiles();
    expect(res.statusCode == 200).toBe(true);
  });

  it('Create Profile', async () => {
    const res = await controller.createProfiles(connection);
    expect(res.statusCode == 201).toBe(true);
  });
});
